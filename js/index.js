/* eslint-disable no-undef */
/* eslint-disable no-console */
// ? trello  

const KEY = '8388a3479d3c43b9b1cae753048ac645';
const TOKEN = 'fba19bbff68ba25186178919df2972f875e9acd7a1611434bff2ae0571b710d0';
const LISTID = '5ce53972ce96a48dd3b2f303';

let cardsIds = (KEY, TOKEN, LISTID) => {
    return fetch(`https://api.trello.com/1/lists/${LISTID}/cards?fields=id&key=${KEY}&token=${TOKEN}`)
    .then(data => data.json())
}
let checkItems = (cardId, KEY, TOKEN) => {
    return fetch(`https://api.trello.com/1/cards/${cardId}/checklists?checkItems=all&checkItem_fields=name%2Cstate&filter=all&fields=idCard%2CcheckItems&key=${KEY}&token=${TOKEN}`)
    .then(res => res.json())
}
let appeadHtml = (arrayItems, cardId) => {
    let html ='';
    arrayItems.map((element)=>{
       html += `<div class="checkItems">
                    <div id="post">
                        <input class="check" ${element['state'] === "complete" ? "checked" : null} data-state="${element['state']}" data-cardId="${cardId}" data-id="${element['id']}" type="checkbox" id=checkbox-${cardId}>
                        <label class="css-label" for=checkbox-${cardId}></label>
                    </div>
                    <div ${element['state'] === "complete" ? "class='checkItem strike'" : "class='checkItem'"}  id="itemName">${element['name']}</div>
                    <div>
                    <a dataCardId="${cardId}" dataId="${element['id']}" href="#" id="delete" class="fa fa-trash-o" style="font-size:26px"></a>
                    </div>
                </div>`;
              });
    $(".items").append(html);
  };
cardsIds(KEY, TOKEN, LISTID).then(cardIds => Promise.all(cardIds.map(cardObj => checkItems(cardObj.id, KEY, TOKEN))))
    .then(checkItems => checkItems.map(response => response.map(checkItem => {
        if (checkItem['checkItems'].length > 0){
            appeadHtml(checkItem.checkItems, checkItem.idCard);}
    }))).catch(error => console.log("error is " + error));

    function checkout(e) {
      e.preventDefault();
      const cardId = $(this).attr('data-cardId');
      const itemId = $(this).attr('data-id');
      const state = this.checked ? 'complete' : 'incomplete';
      fetch(`https://api.trello.com/1/cards/${cardId}/checkItem/${itemId}?state=${state}&key=${KEY}&token=${TOKEN}`, { method: 'PUT' })
        .catch(error => console.log(error));
      $(this).parent().siblings('#itemName').attr('class', this.checked ? 'checkItem strike' : 'checkItem');
    }
    function deleteItem(e) {
      e.preventDefault();
      const cardId = $(this).attr('data-cardId');
      const itemId = $(this).attr('data-id');
      fetch(`https://api.trello.com/1/cards/${cardId}/checkItem/${itemId}?key=${KEY}&token=${TOKEN}`, { method: 'DELETE' })
        .catch(error => console.log(error));
      $(this).parent().parent().remove();
    }
    $(document).on('change', 'input:checkbox', checkout);
    $(document).on('click', 'a', deleteItem);
    let oriVal;
    $(document).on('dblclick', '#itemName', function addInputBox() {
      oriVal = $(this).text();
      $(this).text('');
      $("<input class='inputBox' type='text'>").appendTo(this).focus();
      $(this).children("input[type='text']").val(oriVal);
    });
    $(document).on('focusout', '#itemName > input', function addName() {
      const $this = $(this);
      const itemName = $this.val();
      const cardId = $this.parent().siblings('#post').children('.check').attr('data-cardId');
      const itemId = $this.parent().siblings('#post').children('.check').attr('data-id');
      $this.parent().text($this.val() || oriVal);
      fetch(`https://api.trello.com/1/cards/${cardId}/checkItem/${itemId}?name=${itemName}&key=${KEY}&token=${TOKEN}`, { method: 'PUT' })
        .catch(error => console.log(error));
      $this.remove();
    });
    $(document).on('dblclick', '#addItem', function addItemBox() {
      $(this).text('');
      $("<input class='inputBox1' type='text'>").appendTo(this).focus();
    });
    $(document).on('focusout', '#addItem >input', function addItem() {
      const itemName = $(this).val();
      fetch(`https://api.trello.com/1/checklists/${tempCheckListId}/checkItems?name=${itemName}&pos=bottom&checked=false&key=${KEY}&token=${TOKEN}`, { method: 'POST' })
        .then(res => res.json())
        .then(newItem => appeadHtml([newItem], tempCardId))
        .catch(error => console.log(error));
      $(this).parent().text('Add a new CheckItem');
      $(this).remove();
    });
    